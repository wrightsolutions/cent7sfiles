#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

import argparse
from datetime import datetime as dt
#from os import getenv as osgetenv
from sys import argv,exit,stdin

""" Set values in /tmp/authorized_k.txt based on args.
cat ~/.ssh/id_rsa.pub | python2 ./command_ssh_pubkey.py --command '/bin/netstat -anp'
cat key.pub | ./command_ssh_pubkey.py --command '/bin/netstat -anp' --from '10.0.2.30'

Prints to stdout. Might want to redirect (>) to /tmp/authorized_k.txt
or >> to ~/.ssh/authorized_keys
"""

progfull = argv[0].strip()
from os import path
progbase = path.basename(progfull)
progname = progbase.split('.')[0]
progdir = path.dirname(progbase)

verbosity_global = 1
# Above is set by default to 1. Also see __name__ == '__main__' section later.

SPACER=chr(32)
HASHER=chr(35)
COMMA=chr(44)
DQ=chr(34)
DOTTY=chr(46)
PARENTH_OPEN = chr(40)
PARENTH_CLOSE = chr(41)
# Above are () and next we define {}
BRACES_OPEN = chr(123)
BRACES_CLOSE = chr(125)

ADESC="""Arguments for setting command in .ssh/authorized_keys when given pubkey on stdin """

SSH_OPTIONS="no-port-forwarding{0}no-X11-forwarding{0}no-agent-forwarding{0}no-pty".format(COMMA)

COMMAND_AUTH_TEMPLATE2 = """command={quote}{command_allowed}{quote}{delim}{sshopt} {pubkey}"""
COMMAND_AUTH_TEMPLATE3 = """from={quote}{from_hosts}{quote}{delim}command={quote}{command_allowed}{quote}{delim}{sshopt} {pubkey}"""


def false_unless_true(bool_str):
	""" Process an arg into lowercase true or false 
	based on appropriate interpretation of the string.
	Although argument is labelled 'bool_str' we accept and
	deal with other cases typical of a non validated input
	In argparse might want type=false_unless_true (untested)
	"""
	bool_return = False
        # Do keep False as the default or trace conditions carefully before changing
	try:
		blower = bool_str.strip().lower()
	except:
		# Not a string
		if bool_str is True:
			bool_return = True
		else:
			# False or some other non-string value
			pass
			#print(bool_str,bool_str)
	try:
		blower_int = int(blower)
		if 1 == blower_int:
			bool_return = True
	except:
		if bool_return is False:
			if bool_str is False:
				pass
			else:
				try:
					if blower in ('true', 't', 'yes'):
						bool_return = True
				except:
					EMSG_PREFIX = 'Unexpected value supplied: '
					raise Exception(EMSG_PREFIX+bool_str)

	return bool_return


def false_unless_true_lowercase(bool_str):
	""" Wrapper around false_unless_true where we REALLY want
	a response that is a stringified lowercase true or false
	"""
	bool_tf = false_unless_true(bool_str)
	bool_lower = 'true'
	if bool_tf is False:
		bool_lower = 'false'
	return bool_lower


def string_count_char(str,char_to_count=DOTTY,verbosity=0):
	if str is None or len(str) < 2:
		return 0
	char_count = 0
	stripped = str.strip()
	len_stripped = len(stripped)
        try:
		charless = stripped.replace(char_to_count,'')
		len_charless = len(charless)
		char_count = len_stripped-len_charless
		if verbosity > 1:
			print("char_count={0} obtained as {1}-{2}".format(char_count,
							len_stripped,len_charless))
		char_count = len_stripped-len_charless
	except TypeError:
		raise
	except:
		pass
	return char_count


def authkeyline_from_pubkey(command_allowed,pubkey,sshopt='',from_hosts=None):
	authkeyline = ''
	if from_hosts is None:
		dict_args = {'command_allowed': command_allowed,
			'sshopt': sshopt,
			'pubkey': pubkey,
			'quote': DQ,
			'delim': COMMA,
		}
		authkeyline = COMMAND_AUTH_TEMPLATE2.format(**dict_args)
	else:
		# Similar to previous block only here dict has extra key 'from_hosts'
		dict_args = {'command_allowed': command_allowed,
			'from_hosts': from_hosts,
			'sshopt': sshopt,
			'pubkey': pubkey,
			'quote': DQ,
			'delim': COMMA,
		}
		authkeyline = COMMAND_AUTH_TEMPLATE3.format(**dict_args)
	return authkeyline


if __name__ == '__main__':

	args_all = argv[1:]
	try:
		args_all_joined = SPACER.join(args_all)
	except:
		args_all_joined = args_all

	par = argparse.ArgumentParser(description=ADESC)
	# Avoid type=bool or similar as that kind of processing is after args assigned here.
	par.add_argument('--command', default='',required=True,
				action='store',
				dest='command',
				help="command the account will be allowed to run e.g. '/bin/netstat -anp'")
	par.add_argument('--from', default='',
				action='store',
				dest='from_hosts',
				help="The 'from' item which defines which hosts can run the command")
	args = par.parse_args()

	#installonshutdown = false_unless_true_lowercase(args.installonshutdown)
	command_allowed = args.command.strip()
	from_hosts = args.from_hosts.strip()
	if len(from_hosts) < 3:
		from_hosts = None
	elif string_count_char(from_hosts,DOTTY) < 3:
		# Given an argument but does not look like an ipv4 address so ignore
		#print(string_count_char(from_hosts,DOTTY,2))
		print("## Rejecting --from given as {0}".format(from_hosts))
		from_hosts = None
	else:
		pass

	#command_lower = command_allowed.lower()

	authkeylines = []
	for line in iter(stdin.readlines()):
		#aline = authkeyline_from_pubkey(command_allowed=command_allowed,pubkey=line,
		aline = authkeyline_from_pubkey(command_allowed,line,SSH_OPTIONS,from_hosts)
		if len(aline) > 10:
			authkeylines.append(aline)

	if BRACES_OPEN in authkeylines:
		# Found a '{' which is opener for format() replacement!
		conf_rc = 211
		authkeylines = ''
	elif BRACES_CLOSE in authkeylines:
		# Found a '}' which is closer for format() replacement!
		conf_rc = 212
		authkeylines = ''
	else:
		conf_rc = 200

	idx = 0
	for idx,line in enumerate(authkeylines):
		print(line.rstrip())
	if idx < 1:
		exit(conf_rc)

	conf_rc = 0	# Excellent (and expected) result so 0 return set
	#print("{0}{0} args: {1}".format(HASHER,args_all))
	#dtiso = dt.now().isoformat()

	exit(conf_rc)


""" Working example produced from this script and added to /root/.ssh/authorized_keys on centos 7
command="df -h;touch /tmp/rwtest$(date +%s).txt && /usr/bin/rpm -q salt-minion && systemctl restart salt-minion",no-port-forwarding,no-X11-forwarding,no-agent-forwarding,no-pty ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDrySI0q2D1yKMj3J5YgItNXjD+rpBmRtKc8+IvFJ/H3quh4Aer3tML1IJpiVYFcfRJr9M80QhwxQV6dUpd+iKHeKaFEeDtktYyvY4d+H6AWkcwE+2SHKWnyQtAiOUaVk3+ggonoYeA/waKRkatDOGZG0fqtZo9uwQC86ppfyYHcSoGPpZ/yQfeTjs1iwq/+JcfyTpJnSNk+lBfq1UgqzB9/EBjDBxabt4DRCswNIgXstzRVC4ktfuKJHAFU11yCctnbEcf9s83PaRhHNeSaquj6EFiV6XeM+1t7IDqUWs+sLuj90/t2RyMukt6zJbelydufK3kjZArU0nhEYZwmtefXXZQ51xNK/AYa/Y3L0DVRY5IuiuK1zWiKFCZCbpnoIN48Qo25dy3M6Cn5YOl0zKa8rBTM4ln+ED3cGQLI6Jaqrzx4ibdrlCSKQrUOwBbHYbdF0zHaCkT39lQJl++lrQrvvuu/63buVqG3tsb0Va7NPpqpDxVde8OHKZPGIjelcrazVuE0bQrcgDNJL8tG4e9hWKw50/g+8A7C/NLeYnMQMbkE4Bsj8pj0aTf6hbY6qgKylnXZLGxVLqrMM1/lUpPJVbn93H4Jx2yZRdF8VSwSmpaTGzdQ63h/rMmfcgzbkT1LDLSZso0gY/m0arPsrBkGD9LUBUeN4nM6jOdH6VyXQ== www.wrightsolutions.co.uk/contact

Above semi-related to PermitRootLogin examples:
PermitRootLogin PermitRootLogin without-password   # <-- generally more secure than a plain 'yes'
PermitRootLogin forced-commands-only			# Supported by this helper script in particular
# Remember that restart in systemctl will start a daemon that is not already running (as well as restarting a running daemon)
"""
